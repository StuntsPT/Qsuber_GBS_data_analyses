# New insights on adaptation and population structure of cork oak using genotyping by sequencing


## In this repository you will find:

* A *Makefile* describing the data analysis process
* A *bin* directory, containing some custom scripts as well as symlinks for the used software (tailored for the Docker image)
* A *SPIDs* directory containing *SPID* files from *PGDSpider* to perform file conversions
* A *Popfiles* directory containing several kinds of population information files that are necessary to perform the data analysis
* A *Data* directory containing the `VCF` and `.loci` files used for the data analyses in the paper


## What you won't find here:

* ~~Input data files~~
* Output files (AKA, analyses results)


## See also:

* The [docker hub repository](https://hub.docker.com/r/stunts/q.suber_gbs_data_analyses/) that uses this repository
* The [manuscript preprint](http://biorxiv.org/cgi/content/short/263160v1)
* The [published manuscript](https://doi.org/10.1111/gcb.14497)
