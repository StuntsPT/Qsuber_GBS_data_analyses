# Makefile for $(run_name) GBS dataset analyses
# First off, define some global variables:

# Run name - Set the name for this run
run_name=Qsuber_GBS_01
# Default subset
loci_subset=All_loci
# Original vcf filename
original_vcf=Qsuber_GBS_01.vcf
# Number of threads to use on all the threaded analyses
threads=4
# How much missing data can each of our individuals have (in percentage)?
miss_thres_percent=60
missing_threshold=$(shell echo $(miss_thres_percent) / 101 | bc -l)
# Min "Minor Allele Frequency" allowed:
MAF=0.03
SHORT_MAF=$(shell echo $(MAF) |sed 's|0\.||')
# Max missing data per SNP, **after** individual filtering
MAX_MISSING=0.80
SHORT_MAX_MISSING=$(shell echo $(MAX_MISSING) |sed 's|0\.||')
# Default VCF file to use
VCF_in_use=Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
# Individuals to remove for geospatial analyses (keep the intial ";")
GEO_remove=/Bul11/Bul16/Bul37/Bul5
REMOVAL_LINE=$(shell echo $(GEO_remove) | sed 's|/| --remove-indv |g')
# FDR value for Bayescan analysis
bayescan_FDR=0.05
# Limit for significant SelEstim KLD values
selestim_limit=0.01
# Number of Ks to calculate
K=8
# Number of STRUCTURE replicates per "K"
Reps=20
# Bayesfactor threshold for RONA analysis
ASSOC_BF=15
# Random seed for whateer requestes it
SEED=112358
# Get the current working directory
wd=$(dir $(abspath Makefile))

# Now, set the rules:

all : filtering clustering outliers popgen geospatial rona neutral_clustering selection_clustering


Filtered_VCFs/$(run_name).imiss : ipyrad_outfiles/$(original_vcf)
	# Create new directory for results
	mkdir -p Filtered_VCFs
	
	# Check for missing data
	bin/vcftools --vcf ipyrad_outfiles/$(original_vcf) --missing-indv --out Filtered_VCFs/$(run_name)


Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).vcf : Filtered_VCFs/$(run_name).imiss
	# Check which individuals have too much missing data
	$(eval indivs_to_remove=$(shell bin/select_low_rep_individuals.sh Filtered_VCFs/$(run_name).imiss $(missing_threshold) | sed 's|^| --remove-indv |g' |tr -d '\n' ))
	
	# Remove individuals with too much missing data
	bin/vcftools --vcf ipyrad_outfiles/$(original_vcf) $(indivs_to_remove) --out Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent) --recode
	mv Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).recode.vcf $@


Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.vcf : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).vcf
	# Remove SNPs with too much missing data, very low MAF and more than biallelic
	bin/vcftools --vcf Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent).vcf  --max-missing $(MAX_MISSING) --maf $(MAF) --out Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele --recode --max-alleles 2
	mv Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.recode.vcf $@


Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.vcf
	# Keep only the Center SNP and discarl add other to avoid LD issues
	# Our final VCF file is now produced.
	python3 bin/vcf_parser.py --center-snp -vcf Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele.vcf
	mv Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_bialleleCenterSNP.vcf $@


Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP_GEO.vcf : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
	# Remove individuals that do not make sense on geo-spatial analyses
	bin/vcftools --vcf Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf $(REMOVAL_LINE) --out Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP_GEO --recode
	mv Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP_GEO.recode.vcf $@


Analyses/PCA/$(loci_subset)/$(run_name)_PCA.pdf : $(VCF_in_use)
	# Create a directory to store outputs
	mkdir -p Analyses/PCA/$(loci_subset)
	
	# Perform a PCA analysis from the VCF file
	Rscript bin/snp_pca_static.R $(VCF_in_use) $@ Popfiles/popinfo_names.txt |grep -A 4 '$$eigenval' |tail -n +2 > Analyses/PCA/$(loci_subset)/$(run_name)_eigenvectors.txt
	mv $@.pdf $@


Analyses/Bayescan/$(run_name).geste : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
	# Create a directory to store outputs
	mkdir -p Analyses/Bayescan
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(wd)Popfiles/popinfo_names.txt|g' SPIDs/skeleton.spid > SPIDs/vcf2geste.spid
	echo "" >> SPIDs/vcf2geste.spid
	echo "WRITER_FORMAT=GESTE_BAYE_SCAN" >> SPIDs/vcf2geste.spid
	echo "" >> SPIDs/vcf2geste.spid
	echo "GESTE_BAYE_SCAN_WRITER_DATA_TYPE_QUESTION=SNP" >> SPIDs/vcf2geste.spid
	
	# Perform the file conversion
	java -Xmx1024m -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf -inputformat VCF -outputfile $@ -outputformat GESTE -spid $(wd)SPIDs/vcf2geste.spid


Analyses/Bayescan/$(run_name)_outliers.txt : Analyses/Bayescan/$(run_name).geste
	# We have to 'cd' due to the way bayescan outputs files
	cd Analyses/Bayescan && $(wd)bin/bayescan -snp -threads $(threads) $(wd)Analyses/Bayescan/$(run_name).geste
	Rscript -e 'source("bin/plot_R.r"); plot_bayescan("Analyses/Bayescan/$(run_name).g_fst.txt", FDR=$(bayescan_FDR))' > $@


Analyses/SelEstim/$(run_name).selestim : Analyses/Bayescan/$(run_name).geste
	# Create a directory to store outputs
	mkdir -p Analyses/SelEstim
	
	# Perform the conversion
	bash bin/GESTE2SelEstim.sh Analyses/Bayescan/$(run_name).geste $@


Analyses/SelEstim/$(run_name)_outliers.txt : Analyses/SelEstim/$(run_name).selestim
	# Run SelEstim
	bin/selestim -threads $(threads) -file Analyses/SelEstim/$(run_name).selestim -outputs Analyses/SelEstim/Results -thin 20 -npilot 50 -burnin 10000 -length 1000000 -calibration
	
	# Modify the R script to print the outliers and the KLD plot
	sed 's|FILENAME_HERE|"Analyses/SelEstim/$(run_name)_KLDs.svg"|g' bin/SelEstim.R > Analyses/SelEstim/SelEstim.R
	
	# Run the R script provided by SelEstim with the output modifications
	Rscript -e 'source("Analyses/SelEstim/SelEstim.R");plot.kld(file="Analyses/SelEstim/Results/summary_delta.out", calibration_file="Analyses/SelEstim/Results/calibration/KLD.out", limit=$(selestim_limit))' > $@
	

Analyses/Structure/$(loci_subset)/$(run_name).structure : $(VCF_in_use)
	# Create a directory to store outputs
	mkdir -p Analyses/Structure/$(loci_subset)
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(wd)Popfiles/popinfo_names.txt|g' SPIDs/skeleton.spid > SPIDs/vcf2structure.spid
	echo "" >> SPIDs/vcf2structure.spid
	echo "WRITER_FORMAT=STRUCTURE" >> SPIDs/vcf2structure.spid
	echo "" >> SPIDs/vcf2structure.spid
	echo "STRUCTURE_WRITER_LOCI_DISTANCE_QUESTION=false" >> SPIDs/vcf2structure.spid
	echo "STRUCTURE_WRITER_DATA_TYPE_QUESTION=SNP" >> SPIDs/vcf2structure.spid
	echo "" >> SPIDs/vcf2structure.spid
	echo "STRUCTURE_WRITER_FAST_FORMAT_QUESTION=true" >> SPIDs/vcf2structure.spid
	
	# Perform the file conversion
	java -Xmx1024m -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile $(wd)$(VCF_in_use) -inputformat VCF -outputfile $(wd)$@ -outputformat STRUCTURE -spid $(wd)SPIDs/vcf2structure.spid


Analyses/Structure/$(loci_subset)/$(run_name).maverick : Analyses/Structure/$(loci_subset)/$(run_name).structure
	# Adjust Structure input file to work with MavericK
	cut -f 1,2,7 $< |sed 's/ /\t/g' > $@


Analyses/Structure/$(loci_subset)/MavericK_pilot/merged/outputEvidence.csv : Analyses/Structure/$(loci_subset)/$(run_name).maverick
	# Run MavericK pilot run wrapped under Structure_threader
	bin/structure_threader run -i Analyses/Structure/$(loci_subset)/$(run_name).maverick -o Analyses/Structure/$(loci_subset)/MavericK_pilot -mv bin/MavericK -K $(K) -t $(threads) --pop Popfiles/structure_sorting_popfile.txt --params Popfiles/MavericK_params_pilot.txt --no_tests 1 --no_plots 1


Analyses/Structure/$(loci_subset)/MavericK_tuned/plots/outputQmatrix_ind_K$(K).svg : Analyses/Structure/$(loci_subset)/$(run_name).maverick Analyses/Structure/$(loci_subset)/MavericK_pilot/merged/outputEvidence.csv
	# Run MavericK tuned run wrapped under Structure_threader
	bin/structure_threader run -i Analyses/Structure/$(loci_subset)/$(run_name).maverick -o Analyses/Structure/$(loci_subset)/MavericK_tuned -mv bin/MavericK -K $(K) -t $(threads) --pop Popfiles/structure_sorting_popfile.txt --params Popfiles/MavericK_params_tuned.txt


Analyses/Structure/$(loci_subset)/str_pilot_runs/finished : Analyses/Structure/$(loci_subset)/$(run_name).structure
	# Get number of loci and individuals
	set -e ;\
	NINDS=$$(echo $$(wc -l Analyses/Structure/$(loci_subset)/$(run_name).structure |cut -d " " -f 1) /2 |bc) ;\
	NLOCI=$$(head -n 1 Analyses/Structure/$(loci_subset)/$(run_name).structure | grep -o " " |wc -l) ;\
	sed "s|NINDS|$${NINDS}|" Popfiles/mainparams > Analyses/Structure/$(loci_subset)/mainparams ;\
	sed -i "s|NLOCI|$${NLOCI}|" Analyses/Structure/$(loci_subset)/mainparams
	
	cp Popfiles/extraparams Analyses/Structure/$(loci_subset)/extraparams
	
	# Run STRUCTURE wrapped under Structure_threader
	bin/structure_threader run -i Analyses/Structure/$(loci_subset)/$(run_name).structure -o Analyses/Structure/$(loci_subset)/str_pilot_runs -st $(wd)bin/structure -R $(Reps) -K $(K) -t $(threads) --pop Popfiles/structure_sorting_popfile.txt --extra_opts D=$(SEED)
	touch $@


Analyses/Structure/$(loci_subset)/str_large_runs/finished : Analyses/Structure/$(loci_subset)/str_pilot_runs/finished
	# Draw plots for the 3 Best Ks
	sed -i "s|NUMREPS   200000|NUMREPS   2000000|" Analyses/Structure/$(loci_subset)/mainparams
	set -e ;\
	bestKs=$$($(wd)bin/get_bestK.py $(wd)Analyses/Structure/$(loci_subset)/str_pilot_runs/bestK/evanno.txt) ;\
	bin/structure_threader run -i Analyses/Structure/$(loci_subset)/$(run_name).structure -o Analyses/Structure/$(loci_subset)/str_large_runs -st $(wd)bin/structure -R 1 -Klist $${bestKs} -t $(threads) --pop Popfiles/structure_sorting_popfile.txt --no_tests 1 --extra_opts D=$(SEED)
	touch $@


Analyses/Genepop/$(run_name).txt : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf
	# Create a directory to store outputs
	mkdir -p Analyses/Genepop
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(wd)Popfiles/popinfo_names.txt|g' SPIDs/skeleton.spid > SPIDs/vcf2genepop.spid
	echo "" >> SPIDs/vcf2genepop.spid
	echo "WRITER_FORMAT=GENEPOP" >> SPIDs/vcf2genepop.spid
	echo "" >> SPIDs/vcf2genepop.spid
	echo "GENEPOP_WRITER_DATA_TYPE_QUESTION=SNP" >> SPIDs/vcf2genepop.spid
	echo "" >> SPIDs/vcf2genepop.spid
	
	# Perform the file conversion
	java -Xmx1024m -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile $(wd)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf -inputformat VCF -outputfile $(wd)Analyses/Genepop/$(run_name).txt -outputformat GENEPOP -spid $(wd)SPIDs/vcf2genepop.spid


Analyses/Genepop/$(run_name)_settings.txt :
	# Prepare the settings file
	sed 's|INPUT_FILENAME|$(wd)Analyses/Genepop/$(run_name).txt|' Popfiles/genepop_settings_skel.txt > $@


Analyses/Genepop/$(run_name).txt.DG : Analyses/Genepop/$(run_name).txt Analyses/Genepop/$(run_name)_settings.txt
	# HW tests
	for number in 1 2 3 4 5 ; do \
	echo "MenuOptions=1.$$number" >> $(wd)Analyses/Genepop/$(run_name)_settings.txt ; \
	cd $(wd)Analyses/Genepop && $(wd)bin/genepop settingsFile=$(wd)Analyses/Genepop/$(run_name)_settings.txt ; \
	sed -i 's|^MenuOptions|#&|g' $(wd)Analyses/Genepop/$(run_name)_settings.txt; \
	cd $(wd) ; \
	done


Analyses/Genepop/$(run_name).txt.DIV : Analyses/Genepop/$(run_name).txt Analyses/Genepop/$(run_name)_settings.txt
	# Allele Freqs
	for number in 1 2 ; do \
	echo "MenuOptions=5.$$number" >> $(wd)Analyses/Genepop/$(run_name)_settings.txt ; \
	cd $(wd)Analyses/Genepop && $(wd)bin/genepop settingsFile=$(wd)Analyses/Genepop/$(run_name)_settings.txt ; \
	sed -i 's|^MenuOptions|#&|g' $(wd)Analyses/Genepop/$(run_name)_settings.txt; \
	cd $(wd) ; \
	done
	

Analyses/Genepop/$(run_name).txt.FST : Analyses/Genepop/$(run_name).txt Analyses/Genepop/$(run_name)_settings.txt
	# F-statistics
	for number in 1 2 ; do \
	echo "MenuOptions=6.$$number" >> $(wd)Analyses/Genepop/$(run_name)_settings.txt ; \
	cd $(wd)Analyses/Genepop && $(wd)bin/genepop settingsFile=$(wd)Analyses/Genepop/$(run_name)_settings.txt ; \
	sed -i 's|^MenuOptions|#&|g' $(wd)Analyses/Genepop/$(run_name)_settings.txt; \
	cd $(wd) ; \
	done


Analyses/Genepop/GEO/$(run_name).txt : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP_GEO.vcf Popfiles/coordinates.txt
	# Create a directory to store outputs
	mkdir -p Analyses/Genepop/GEO
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(wd)Popfiles/popinfo_names_GEO.txt|g' SPIDs/skeleton.spid > SPIDs/vcf2genepop.spid
	echo "" >> SPIDs/vcf2genepop.spid
	echo "WRITER_FORMAT=GENEPOP" >> SPIDs/vcf2genepop.spid
	echo "" >> SPIDs/vcf2genepop.spid
	echo "GENEPOP_WRITER_DATA_TYPE_QUESTION=SNP" >> SPIDs/vcf2genepop.spid
	echo "" >> SPIDs/vcf2genepop.spid
	
	# Perform the file conversion
	java -Xmx1024m -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile $(wd)Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP_GEO.vcf -inputformat VCF -outputfile $(wd)Analyses/Genepop/GEO/$(run_name).txt -outputformat GENEPOP -spid $(wd)SPIDs/vcf2genepop.spid
	
	# Replace pop names with coordinates
	python3 bin/coord_replacer.py Popfiles/coordinates.txt Analyses/Genepop/GEO/$(run_name).txt > Analyses/Genepop/GEO/$(run_name)_GEO.txt
	mv $(wd)Analyses/Genepop/GEO/$(run_name)_GEO.txt $@


Analyses/Genepop/GEO/$(run_name).txt.ISO : Analyses/Genepop/GEO/$(run_name).txt
	# Prepare the settings file
	sed 's|INPUT_FILENAME|$(wd)Analyses/Genepop/GEO/$(run_name).txt|' $(wd)Popfiles/genepop_settings_skel.txt > $(wd)Analyses/Genepop/GEO/$(run_name)_settings.txt
	echo "MenuOptions=6.6" >> $(wd)Analyses/Genepop/GEO/$(run_name)_settings.txt
	
	# Run Genpop
	cd $(wd)Analyses/Genepop/GEO && $(wd)bin/genepop settingsFile=$(wd)Analyses/Genepop/GEO/$(run_name)_settings.txt && rm LOCUS*
	cd $(wd)


Analyses/Baypass/$(run_name).GEOgeste : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP_GEO.vcf
	# Create a directory to store outputs
	mkdir -p Analyses/Baypass
	
	# Create a SPID from the skeleton
	sed 's|path_to_popfile|$(wd)Popfiles/popinfo_names_GEO.txt|g' SPIDs/skeleton.spid > SPIDs/vcf2GEOgeste.spid
	echo "" >> SPIDs/vcf2GEOgeste.spid
	echo "WRITER_FORMAT=GESTE_BAYE_SCAN" >> SPIDs/vcf2GEOgeste.spid
	echo "" >> SPIDs/vcf2GEOgeste.spid
	echo "GESTE_BAYE_SCAN_WRITER_DATA_TYPE_QUESTION=SNP" >> SPIDs/vcf2GEOgeste.spid
	
	# Perform the file conversion
	java -Xmx1024m -Xms512m -jar bin/PGDSpider2-cli.jar -inputfile Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP_GEO.vcf -inputformat VCF -outputfile Analyses/Baypass/$(run_name).GEOgeste -outputformat GESTE -spid $(wd)SPIDs/vcf2GEOgeste.spid


Analyses/Baypass/$(run_name).baypass : Analyses/Baypass/$(run_name).GEOgeste
	# Convert geste to baypass format
	python3 bin/geste2baypass.py Analyses/Baypass/$(run_name).GEOgeste $@


Analyses/Baypass/$(run_name)_associations_summary.txt : Analyses/Baypass/$(run_name).baypass Popfiles/ENVFILE
	# Prepare the wrapper program by defining the required variables
	sed 's|source("")|source("$(wd)bin/baypass_utils.R")|' bin/Baypass_workflow.R > Analyses/Baypass/Baypass_workflow.R
	sed -i 's|baypass_executable = ""|baypass_executable = "$(wd)bin/g_baypass"|' Analyses/Baypass/Baypass_workflow.R
	sed -i 's|popname_file = ""|popname_file = "$(wd)Popfiles/popnames_single_GEO.txt"|' Analyses/Baypass/Baypass_workflow.R
	sed -i 's|envfile = ""|envfile = "$(wd)Popfiles/ENVFILE"|' Analyses/Baypass/Baypass_workflow.R
	sed -i 's|geno_file = ""|geno_file = "$(wd)Analyses/Baypass/$(run_name).baypass"|' Analyses/Baypass/Baypass_workflow.R
	sed -i 's|prefix = ""|prefix = "$(run_name)"|' Analyses/Baypass/Baypass_workflow.R
	sed -i 's?num_pops =?num_pops = $(shell head -n 3 Analyses/Baypass/$(run_name).GEOgeste | tail -n 1 |sed "s/.*=//")?' Analyses/Baypass/Baypass_workflow.R
	sed -i 's?num_SNPs =?num_SNPs = $(shell head -n 1 Analyses/Baypass/$(run_name).GEOgeste | sed "s/.*=//")?' Analyses/Baypass/Baypass_workflow.R
	sed -i 's|num_threads =|num_threads = $(threads)|' Analyses/Baypass/Baypass_workflow.R
	sed -i 's|scale_cov = TRUE|scale_cov = TRUE|' Analyses/Baypass/Baypass_workflow.R
	
	# Run Baypass
	cd Analyses/Baypass && Rscript Baypass_workflow.R
	
	# Gather a summary of associations
	python3 bin/associations_gatherer.py Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_betai.out $(ASSOC_BF) > $@


Analyses/pyRona/$(run_name)_RONA_rpc26.pdf : Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_pij.out Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_betai.out Popfiles/ENVFILE_rpc26 Popfiles/ENVFILE Popfiles/popnames_single_GEO.txt
	# Create directory for outputs
	mkdir -p Analyses/pyRona
	# Run pyRona.py
	python3 bin/pyRona.py -pc $(wd)Popfiles/ENVFILE -fc $(wd)Popfiles/ENVFILE_rpc26 -be $(wd)Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_betai.out -pij $(wd)Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_pij.out -pop $(wd)Popfiles/popnames_single_GEO.txt -bf $(ASSOC_BF) -outliers 0 -no-plots -out $(wd)$@ > $(wd)Analyses/pyRona/RONA_rpc26_summary.txt


Analyses/pyRona/$(run_name)_RONA_rpc85.pdf : Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_pij.out Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_betai.out Popfiles/ENVFILE_rpc85 Popfiles/ENVFILE Popfiles/popnames_single_GEO.txt
	# Create directory for outputs
	mkdir -p Analyses/pyRona
	# Run pyRona.py
	python3 bin/pyRona.py -pc $(wd)Popfiles/ENVFILE -fc $(wd)Popfiles/ENVFILE_rpc85 -be $(wd)Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_betai.out -pij $(wd)Analyses/Baypass/mcmc_aux/$(run_name)_mcmc_aux_summary_pij.out -pop $(wd)Popfiles/popnames_single_GEO.txt -bf $(ASSOC_BF) -outliers 0 -no-plots -out $(wd)$@ > $(wd)Analyses/pyRona/RONA_rpc85_summary.txt


Filtered_VCFs/$(run_name)_neutrals.vcf : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf Analyses/Baypass/$(run_name)_associations_summary.txt Analyses/Bayescan/$(run_name)_outliers.txt Analyses/SelEstim/$(run_name)_outliers.txt
	python3 bin/outlier_removal.py neutral -o Analyses/Bayescan/$(run_name)_outliers.txt Analyses/SelEstim/$(run_name)_outliers.txt -a Analyses/Baypass/$(run_name)_associations_summary.txt -v $< > $@


Filtered_VCFs/$(run_name)_selection.vcf : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf Analyses/Baypass/$(run_name)_associations_summary.txt Analyses/Bayescan/$(run_name)_outliers.txt Analyses/SelEstim/$(run_name)_outliers.txt
	python3 bin/outlier_removal.py selection -o Analyses/Bayescan/$(run_name)_outliers.txt Analyses/SelEstim/$(run_name)_outliers.txt -a Analyses/Baypass/$(run_name)_associations_summary.txt -v $< > $@


Analyses/BLASTs/$(run_name)_outlier_marker_names.txt : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf Analyses/Bayescan/$(run_name)_outliers.txt Analyses/SelEstim/$(run_name)_outliers.txt
	# Create a drectory for outputs
	mkdir -p Analyses/BLASTs
	# Parse the files and get the names
	python3 bin/outlier_removal.py fasta -o $(word 2,$^) $(word 3,$^) -v $< > $@


Analyses/BLASTs/$(run_name)_outlier.fasta : Analyses/BLASTs/$(run_name)_outlier_marker_names.txt ipyrad_outfiles/$(subst .vcf,.loci,$(original_vcf))
	# Make a FASTA with the outlier loci sequences
	python3 bin/loci_consensus.py -in $(word 2,$^) -l $< -o $@


Analyses/BLASTs/$(run_name)_assoc_marker_names.txt : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf Analyses/Baypass/$(run_name)_associations_summary.txt
	# Create a drectory for outputs
	mkdir -p Analyses/BLASTs
	# Parse the files and get the names
	python3 bin/outlier_removal.py fasta -a $(word 2,$^) -v $< > $@


Analyses/BLASTs/$(run_name)_assoc.fasta : Analyses/BLASTs/$(run_name)_assoc_marker_names.txt ipyrad_outfiles/$(subst .vcf,.loci,$(original_vcf))
	# Make a FASTA with the associated loci sequences
	python3 bin/loci_consensus.py -in $(word 2,$^) -l $< -o $@


Analyses/BLASTs/Q.lobata/Q.lobata.gff :
	# Create a directory for the GFF annotation file
	mkdir -p Analyses/BLASTs/Q.lobata
	# Get the GFF annotation file - WARNING - 1.2Gb file:
	wget https://ucla.box.com/shared/static/bhtchvyq59iw2ljyfg4f5250efv5ascl.gz -O $@.gz
	gunzip -c $@.gz > $@


Analyses/BLASTs/Q.lobata/Q.lobata.fasta :
	# Create a directory for the Q. lobata fasta file
	mkdir -p Analyses/BLASTs/Q.lobata
	# Get the fasta file for Q. lobata
	wget https://ucla.box.com/shared/static/asburukgkfgwzwxpzwb0ig1x88lmg1j0.gz -O $@.gz
	gunzip -c $@.gz > $@


Analyses/BLASTs/Q.lobata/Q.lobata.nin : Analyses/BLASTs/Q.lobata/Q.lobata.fasta
	# Turn the fasta file into a BLAST db
	makeblastdb -in $< -parse_seqids -dbtype nucl -out $(basename $@)


Analyses/BLASTs/$(run_name)_assoc_qlobata.tab : Analyses/BLASTs/Q.lobata/Q.lobata.nin Analyses/BLASTs/$(run_name)_assoc.fasta
	# Run a BLAST query of the associated sequences against the Q. lobata database
	blastn -db $(basename $<) -query $(word 2,$^) -out $@ -outfmt "6 qseqid sseqid qlen evalue nident length sstart send" -num_threads $(threads) -evalue 0.000001 -max_target_seqs 1


Analyses/BLASTs/$(run_name)_outlier_qlobata.tab : Analyses/BLASTs/Q.lobata/Q.lobata.nin Analyses/BLASTs/$(run_name)_outlier.fasta
	# Run a BLAST query of the outlier sequences against the Q. lobata database
	blastn -db $(basename $<) -query $(word 2,$^) -out $@ -outfmt "6 qseqid sseqid qlen evalue nident length sstart send" -num_threads $(threads) -evalue 0.000001 -max_target_seqs 1


Analyses/BLASTs/$(run_name)_assoc_annot_qlobata.tsv : Analyses/BLASTs/$(run_name)_assoc_qlobata.tab Analyses/BLASTs/Q.lobata/Q.lobata.gff
	# Turn BLAST results and GFF into summary annotations.
	# "Blasts" the sequences of SNPs with associations, finds annotations, removes "unknown products" and purges duplicate entries.
	# Also sorts by VCF loci name.
	bin/bl_gff_2_annotation.py $< $(word 2,$^) |grep "Note" |grep -v -i "unknown" | (read -r; printf "%s\n" "$$REPLY"; sort -V) |uniq > $@


Analyses/BLASTs/$(run_name)_outlier_annot_qlobata.tsv : Analyses/BLASTs/$(run_name)_outlier_qlobata.tab Analyses/BLASTs/Q.lobata/Q.lobata.gff
	# Turn BLAST results and GFF into summary annotations.
	# "Blasts" the sequences of outlier SNPs, finds annotations, removes "unknown products" and purges duplicate entries.
	# Also sorts by VCF loci name.
	bin/bl_gff_2_annotation.py $< $(word 2,$^) |grep -v -i "unknown" | (read -r; printf "%s\n" "$$REPLY"; sort -V) |uniq > $@


Analyses/BLASTs/$(run_name)_assoc_annot_qlobata_complete.tsv : Analyses/BLASTs/$(run_name)_assoc_annot_qlobata.tsv Analyses/Baypass/$(run_name)_associations_summary.txt
	# Add an associated variable field to the annotations file.
	cut -d "#" -f 2 $< |cut  -f 1| tail -n +3 |sed 's/^.*/\t&\t/' > Analyses/BLASTs/$(run_name)_temp_data.txt
	while read f ; \
		do \
		_line=$$(grep --perl-regex "\t$$f\t" $(word 2,$^) |cut -f 1 | tr '\n' '\t') ; \
		echo $$_line | sed "s/.*/$$f\t&/" ; \
		done < Analyses/BLASTs/$(run_name)_temp_data.txt | sed 1i"\\\nSNP name\tAssociations" | paste $< - > $@
	rm Analyses/BLASTs/$(run_name)_temp_data.txt

# "Meta" rules
.PHONY : all clean filtering clustering neutral_clustering outliers popgen geospatial rona PCA neutral_pca structure neutral_structure blast

clean :
	# Clear EVERYTHING!
	rm -rf Analyses/*
	rm -rf Filtered_VCFs/*

filtering : Filtered_VCFs/$(run_name)_indiv$(miss_thres_percent)_miss$(SHORT_MAX_MISSING)_maf$(SHORT_MAF)_biallele_CenterSNP.vcf

clustering : PCA structure

neutral_clustering : neutral_pca neutral_structure

selection_clustering : selection_pca selection_structure
	
PCA : Analyses/PCA/$(loci_subset)/$(run_name)_PCA.pdf 
	
structure : Analyses/Structure/$(loci_subset)/MavericK_tuned/plots/outputQmatrix_ind_K$(K).svg Analyses/Structure/$(loci_subset)/MavericK_pilot/merged/outputEvidence.csv Analyses/Structure/$(loci_subset)/str_pilot_runs/finished Analyses/Structure/$(loci_subset)/str_large_runs/finished

outliers : Analyses/Bayescan/$(run_name)_outliers.txt Analyses/SelEstim/$(run_name)_outliers.txt

popgen : Analyses/Genepop/$(run_name).txt.DG Analyses/Genepop/$(run_name).txt.DIV Analyses/Genepop/$(run_name).txt.FST

geospatial : Analyses/Genepop/GEO/$(run_name).txt.ISO Analyses/Baypass/$(run_name)_associations_summary.txt

rona : Analyses/pyRona/$(run_name)_RONA_rpc26.pdf Analyses/pyRona/$(run_name)_RONA_rpc85.pdf

blast : Analyses/BLASTs/$(run_name)_outlier.fasta Analyses/BLASTs/$(run_name)_assoc.fasta Analyses/BLASTs/$(run_name)_assoc_annot_qlobata_complete.tsv Analyses/BLASTs/$(run_name)_outlier_annot_qlobata.tsv

neutral_pca : Filtered_VCFs/$(run_name)_neutrals.vcf
	$(MAKE) PCA VCF_in_use=$< loci_subset=Neutrals

neutral_structure : Filtered_VCFs/$(run_name)_neutrals.vcf
	$(MAKE) structure VCF_in_use=$< loci_subset=Neutrals

selection_pca : Filtered_VCFs/$(run_name)_selection.vcf
	$(MAKE) PCA VCF_in_use=$< loci_subset=Selection

selection_structure : Filtered_VCFs/$(run_name)_selection.vcf
	$(MAKE) structure VCF_in_use=$< loci_subset=Selection
