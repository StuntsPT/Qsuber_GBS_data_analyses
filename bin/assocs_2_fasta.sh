#!/bin/bash

# Usage: assocs_2_fasta.sh assocaitions_summary_file file.vcf assocs_greppable_file.txt file.loci output_file.fasta

# Example: ./assocs_2_fasta.sh Analyses/Baypass/associations_summary.txt Filtered_VCFs/Qsuber_GBS_indiv30_miss80_maf03_biallele_CenterSNP.vcf associations_greppable.txt qsuber_03.loci associations.fasta

temp_file="/tmp/assoc_snps.txt"

cut -f 2 ${1} | sort -n | uniq > ${temp_file}

while read f       
do
  numb=$(echo "$f + 11" | bc)
  echo $(sed "${numb}q;d" ${2} |cut -f 1 ) - 1 |bc
done < ${temp_file} |sed 's/.*/|&|/g' > ${3}

grep -B 1 -f ${3} ${4} |grep -v '//\|^--'  | sed 's/ \+/\t/g' |cut -f 2 | paste ${temp_file}  - |sed 's/^/>/' |tr '	' '\n' > ${5}
