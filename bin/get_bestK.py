#!/usr/bin/env python3

def read_evanno(evanno_filename):
    evanno = open(evanno_filename, 'r')
    delta_Ks = []
    for lines in evanno:
        if lines.startswith(("1", "2", "3", "4", "5", "6", "7", "8", "9", "0")):
            lines = lines.split()
            delta_Ks.append(float(lines[6].replace("NA", "0")))

    evanno.close()
    return delta_Ks


def select_greater(delta_Ks, how_many=3):
    best_Ks = []
    for i in range(how_many):
        max_index =delta_Ks.index(max(delta_Ks))
        best_Ks.append(max_index + 1)
        delta_Ks[max_index] = 0.0
    return best_Ks


if __name__ == "__main__":
    from sys import argv
    deltas = read_evanno(argv[1])
    final = ""
    for Ks in select_greater(deltas):
        final += str(Ks) + " "
    print(final.strip())
