#### Data proprieties
headerRow_on	f
popCol_on	t
ploidyCol_on	f
ploidy	2
missingData	-9


#### Model parameters
Kmin	1
Kmax	4
admix_on	t
fixAlpha_on	f
alpha	1.0
alphaPropSD	0.10


#### Simulation parameters
exhaustive_on	f
mainRepeats	1
mainBurnin	500
mainSamples	5000

thermodynamic_on	f
thermodynamicRungs	20
thermodynamicBurnin	5000
thermodynamicSamples	50000


#### Basic output proprieties
outputLog_on	t
outputLikelihood_on	t
outputQmatrix_ind_on	t
outputQmatrix_pop_on	t
outputEvidence_on	t
outputEvidenceDetails_on	t


#### Output location
